package com.gestion.employeemanager.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gestion.employeemanager.exception.UserNotFoundException;
import com.gestion.employeemanager.model.Employee;
import com.gestion.employeemanager.repository.EmployeeRepository;

@Service
public class EmployeeService {
	
	private final EmployeeRepository employeeRepository;
	
	@Autowired
	public EmployeeService(EmployeeRepository employeeRepository) {
		super();
		this.employeeRepository = employeeRepository;
	}
	
	/**
	 * Add an employee in database
	 * @param employee
	 * @return employee
	 */
	public Employee addEmployee(Employee employee) {
		//create uuid for add an employee
		employee.setEmployeeCode(UUID.randomUUID().toString());
		return employeeRepository.save(employee);
	}
	
	/**
	 * Return a list of employee
	 * @return List
	 */
	public List<Employee> findAllEmployees(){
		return employeeRepository.findAll();
	}
	
	/**
	 * update information of employee
	 * @param employee
	 * @return employee
	 */
	public Employee update(Employee employee) {
		return employeeRepository.save(employee);
	}
	
	/**
	 * find an employee by his id
	 * @param id
	 * @return employee or null
	 */
	public Employee findEmployeeById(Long id) {
		return employeeRepository.findEmployeeById(id).orElseThrow(() -> new UserNotFoundException("User with id : " + id + "was not found"));
	}
	
	/**
	 * delete an employee in database
	 * @param id
	 */
	public void deleteEmployee(Long id) {
		employeeRepository.deleteById(id);
	}
	

}
