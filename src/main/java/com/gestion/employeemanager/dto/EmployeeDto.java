package com.gestion.employeemanager.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeDto {
	
	private String name;	
	
	private String email;
	
	private String jobTitle;
	
	private String phone;
	
	private String imageUrl;

	public EmployeeDto() {
		super();		
	}

	public EmployeeDto(String name, String email, String jobTitle, String phone, String imageUrl) {
		super();
		this.name = name;
		this.email = email;
		this.jobTitle = jobTitle;
		this.phone = phone;
		this.imageUrl = imageUrl;
	}
	
}
