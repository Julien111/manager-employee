package com.gestion.employeemanager.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gestion.employeemanager.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long > {
	
	Optional<Employee> findEmployeeById(Long id);
}
