package com.gestion.employeemanager.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Employee implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	@Column(nullable = false, updatable =  false)
	private Long id;
	
	private String name;
	
	@Column(unique = true)
	private String email;
	
	private String jobTitle;
	
	private String phone;
	
	private String imageUrl;
	
	@Column(nullable = false, updatable =  false)
	private String employeeCode;
	
	public Employee() {
		super();		
	}
	
	public Employee(String name, String email, String jobTitle, String phone, String imageUrl) {
		super();		
		this.name = name;
		this.email = email;
		this.jobTitle = jobTitle;
		this.phone = phone;
		this.imageUrl = imageUrl;		
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", email=" + email + ", jobTitle=" + jobTitle + ", phone="
				+ phone + ", imageUrl=" + imageUrl + ", employeeCode=" + employeeCode + "]";
	}	

}
