package com.gestion.employeemanager.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gestion.employeemanager.dto.EmployeeDto;
import com.gestion.employeemanager.model.Employee;
import com.gestion.employeemanager.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	private final EmployeeService employeeService;

	public EmployeeController(EmployeeService employeeService) {
		this.employeeService = employeeService;				
	}
	
	@GetMapping("/all")
	public	ResponseEntity<List<Employee>> getAllEmployees(){
		List<Employee> employees = employeeService.findAllEmployees();
		return new ResponseEntity<>(employees, HttpStatus.OK);
	}
	
	@GetMapping("/find/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Long id){
		Employee employee = employeeService.findEmployeeById(id);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}
	
	@PostMapping("/create")
	public ResponseEntity<Employee> createEmployee(@RequestBody EmployeeDto employeeDto){
		Employee newEmployee = new Employee(employeeDto.getName(), employeeDto.getEmail(), employeeDto.getJobTitle(), employeeDto.getPhone(), employeeDto.getImageUrl());
		Employee addEmployee = employeeService.addEmployee(newEmployee);
		return new ResponseEntity<>(addEmployee, HttpStatus.OK);
	}
	
	@PutMapping("/edit/{id}")
	public ResponseEntity<Employee> updateEmployee(@RequestBody EmployeeDto employeeDto, @PathVariable("id") Long id){
		Employee newEmployee = employeeService.findEmployeeById(id);
		//test
		if(employeeDto.getName() != null && !employeeDto.getName().isBlank()) {
			newEmployee.setName(employeeDto.getName());			
		}
		if(employeeDto.getEmail() != null && !employeeDto.getEmail().isBlank()) {
			newEmployee.setEmail(employeeDto.getEmail());
		}
		if(employeeDto.getImageUrl() != null && !employeeDto.getImageUrl().isBlank()) {
			newEmployee.setImageUrl(employeeDto.getImageUrl());
		}
		if(employeeDto.getPhone() != null && !employeeDto.getPhone().isBlank()) {
			newEmployee.setPhone(employeeDto.getPhone());
		}
		if(employeeDto.getJobTitle() != null && !employeeDto.getJobTitle().isBlank()) {
			newEmployee.setJobTitle(employeeDto.getJobTitle());
		}
				
		Employee updateEmployee = employeeService.update(newEmployee);
		return new ResponseEntity<>(updateEmployee, HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable("id") Long id){
		employeeService.deleteEmployee(id);
		return new ResponseEntity<>( HttpStatus.OK);
	}

}
